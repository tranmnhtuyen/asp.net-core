﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThucTap.Models;
using Excel = Microsoft.Office.Interop;

namespace ThucTap.Controllers
{
    public class UserController : Controller
    {
        DB_USEREntities db = new DB_USEREntities();
        // GET: User
        public ActionResult AddUser()
        {
            UserModel obj = new UserModel();
            //ViewBag.listUser = obj.find
            return View("AddUser", obj);
        }

        [HttpPost]
        public ActionResult AddUser(UserModel obj)
        {
            var UserRecord = (from item in db.Users where item.Code == obj.Code select item).FirstOrDefault();
           
                if (UserRecord == null)
                {
                    db.Users.Add(new User() { Code = obj.Code, Name = obj.Name, Password = obj.Password, InsertUserId = Session["Name"].ToString(), InsertDate = @DateTime.Now.ToString() });
                    db.SaveChanges();
                    ViewBag.Message = "Thêm tài khoản thành công!";
                    return View("AddUser", obj);

                }
                else
                {
                    ViewBag.ErrorMessage = "Code đã được sử dụng. Vui lòng đặt Code khác!";

                return View();
                }
                          
        }



        public ActionResult DisplayUser()
        {
            
            var UserRecords = db.Users.ToList();
            return View("DisplayUser", UserRecords);
        }

        public ActionResult EditUser(int idUser)
        {

            var UserRecord = (from item in db.Users where item.ID == idUser select item).First();
           
            return View("EditUser", UserRecord);
        }

        [HttpPost]
        public ActionResult EditUser(User obj)
        {
          
           
                var UserRecord = (from item in db.Users where item.ID == obj.ID select item).First();

                UserRecord.Code = obj.Code;
                UserRecord.Name = obj.Name;
                UserRecord.Password = obj.Password;
                UserRecord.LastEditDate = DateTime.Now.ToString();
                UserRecord.LastEditUser = Session["Name"].ToString();
                db.SaveChanges();
                ViewBag.EditMessage = "Sửa thành công! " + UserRecord.Code;
                var UserRecords = db.Users.ToList();
                return View("DisplayUser", UserRecords);
           
        }

        public ActionResult DeleteUser(int idUser)
        {
            var UserRecord = (from item in db.Users where item.ID == idUser select item).First();

            if (UserRecord != null)
            {
                ViewBag.DeleteMessage = "Xóa thành công!" + UserRecord.Name;
                db.Users.Remove(UserRecord);
                db.SaveChanges();

                var UserRecords = db.Users.ToList();
                return View("DisplayUser", UserRecords);
            }
            else
            {
                var UserRecords = db.Users.ToList();
                return View("DisplayUser", UserRecords);
            }
          
        }
        public ActionResult Export()
        {
            using (var workbook = new XLWorkbook()) {
                var worksheet = workbook.Worksheets.Add("User");
                var currentRow = 1;

                worksheet.Cell(currentRow, 1).Value = "ID";
                worksheet.Cell(currentRow, 2).Value = "Code";
                worksheet.Cell(currentRow, 3).Value = "Name";
                worksheet.Cell(currentRow, 4).Value = "Password";
                worksheet.Cell(currentRow, 5).Value = "InsertUserId";
                worksheet.Cell(currentRow, 6).Value = "InsertDate";
                worksheet.Cell(currentRow, 7).Value = "LastEditUser";
                worksheet.Cell(currentRow, 8).Value = "LastEditDate";


                foreach ( var u in db.Users)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = u.ID;
                    worksheet.Cell(currentRow, 2).Value = u.Code;
                    worksheet.Cell(currentRow, 3).Value = u.Name;
                    worksheet.Cell(currentRow, 4).Value = u.Password;
                    worksheet.Cell(currentRow, 5).Value = u.InsertUserId;
                    worksheet.Cell(currentRow, 6).Value = u.InsertDate;
                    worksheet.Cell(currentRow, 7).Value = u.LastEditUser;
                    worksheet.Cell(currentRow, 8).Value = u.LastEditDate;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var conten = stream.ToArray();
                    return File(
                        conten,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "User.xlsx"
                        );
                }
            }
            
        }

        public ActionResult CountUser( UserModel user)
        {
            System.Web.Security.MembershipUser u = System.Web.Security.Membership.GetUser(user.Code);
            return View();
        }
        //public ActionResult Export()
        //{
        //    var data = db.Users.ToList();
        //    return View(data);
        //}

        //[HttpPost]
        //public ActionResult Export()
        //{
        //    List<UserModel> emplist = db.Users.Select(u => new UserModel
        //    {
        //        ID = u.ID,
        //        Code = u.Code,
        //        Name = u.Name,
        //        Password = u.Password,
        //        InsertUserId = u.InsertUserId,
        //        InsertDate = u.InsertDate,
        //        LastEditUser = u.LastEditUser,
        //        LastEditDate = u.LastEditDate
        //    }).ToList();

        //    return View(emplist);
        //}


        //public ActionResult ExportToExcel()
        //{
        //    List<UserModel> emplist = db.Users.Select(u => new UserModel
        //    {
        //        ID = u.ID,
        //        Code = u.Code,
        //        Name = u.Name,
        //        Password = u.Password,
        //        InsertUserId = u.InsertUserId,
        //        InsertDate = u.InsertDate,
        //        LastEditUser = u.LastEditUser,
        //        LastEditDate = u.LastEditDate
        //    }).ToList();

        //    //ExportPackage pck = new ExportPackage();
        //    //ExcelWorksheet ws = new pck.Workbook.Worksheet.Add("Report");
               
        //    return View();
        //}



    }
}