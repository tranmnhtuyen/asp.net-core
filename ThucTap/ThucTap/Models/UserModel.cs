﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ThucTap.Models
{
    public class UserModel
    {
        public int ID { get; set; }
        [Required]
        public string Code { get; set; }

        public string Name { get; set; }
        public string Password { get; set; }
        public string InsertUserId { get; set; }
        public string InsertDate { get; set; }
        public string LastEditUser { get; set; }
        public string LastEditDate { get; set; }
    }
}