﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThucTap.Models;

namespace ThucTap.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Autherise(ThucTap.Models.User userModel)
        {
            
               
                using (DB_USEREntities db = new DB_USEREntities())
                {
                    var userDetails = db.Users.Where(x => x.Code == userModel.Code && x.Password == userModel.Password).FirstOrDefault();
                    if(userModel.Code == null || userModel.Password == null)
                    {
                    ViewBag.LoginMessage = "Không được bỏ trống Code và Password";
                    return View("Index", userModel);
                }
                else
                {
                    if (userDetails == null)

                    {
                        ViewBag.LoginMessage = "Sai Code hoặc Password";
                        return View("Index", userModel);
                    }

                    else
                    {
                        Session["Name"] = userDetails.Name;
                        return RedirectToAction("Index", "Home");
                    }
                }
                    

                }
        }        

        public ActionResult LogOut()
        {
            Session.Abandon();
            Session.Clear();
            //Session.RemoveAll();
            //System.Web.Security.FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Login");
        }
    }
}